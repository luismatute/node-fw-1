/*
	Server JS
	Developers:
		Luis Matute - luis.matute@me.com
	Description:
		This is the server configuration.
*/

// Module dependencies and app instantiation
	var
	  express 	= require('express'),
	  http 			= require('http'),
	  path 			= require('path')
	  mongoose 	= require('mongoose'),
	  passport	= require('passport'),
	  util			= require('./config/utilities'),

	  // Instantiate the app
	  app 			= express(),
	  env 			= app.get('env');

// bootstrap passport config
	require('./config/passport')(passport);

// Requiring Environment Vars
	app.locals( require('./config/environments')[env] );

// Start Utils
	util.startup(__dirname);

// all environments
	app.configure(function(){
	  app
		  .use(express.compress({
		  	// Should be placed before express.static
				// Compress response data with gzip / deflate
		    filter: function (req, res) {
		      return /json|text|javascript|css/.test(res.getHeader('Content-Type'))
		    },
		    level: 9
		  }))
			.set('view engine', 'jade')
			.set('env', env)
			.use(express.favicon())
			.use(express.logger('dev'))
			.use(express.json())
			.use(express.urlencoded())
			.use(express.methodOverride())
			.use(app.router)
			.use(require('less-middleware')({ src: path.join(__dirname, 'public') }))
			.use('/common', express.static(path.join(__dirname, 'common/assets')))
			// use passport session
			.use(passport.initialize())
			.use(passport.session());
	});

// development only
	app.configure('development', function(){
	  app.use(express.errorHandler());  // This will show us a detailed error in development
	});

// Routes
	require('./config/routes')(app);

// assume 404 since no middleware responded
  app.use(function(req, res, next){
    res.status(404).render(path.join(__dirname, 'common/views/404'), {
      url: req.originalUrl,
      error: 'Not found'
    });
  });

// Connect to mongo and then start server
	// connectToMongo(function(){
	//   app.listen(app.locals.port, function () {
	//     console.log(
	// 	  	'Express server listening on port ' + app.locals.port + '\n'
	// 	  	+ 'With ' + env.toUpperCase() + ' Environment settings loaded'
	// 	  );
	//   });
	// });

	app.listen(app.locals.port, function () {
    console.log(
	  	'Express server listening on port ' + app.locals.port + '\n'
	  	+ 'With ' + env.toUpperCase() + ' Environment settings loaded'
	  );
  });

// Function that connects to mongo
	function connectToMongo(callback) {
		var
			server = app.locals.mongo.hosts[0].serverName,
			connect = function () {
			  var options = { server: { socketOptions: { keepAlive: 1 } } }
			  mongoose.connect(server, options)
			};

		// Connect to mongo
		connect();

		// Show when we have connected
	  mongoose.connection.on('open', function(){
	    console.log('Connected to ' + server);
	    callback();
	  });

	  // Error handler
		mongoose.connection.on('error', function (err) {
		  console.log(err)
		});

		// Reconnect when closed
		mongoose.connection.on('disconnected', function () {
		  connect();
		})
	}