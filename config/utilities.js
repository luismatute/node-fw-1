/*
	Utilities
*/

var
	fs 						= require('fs'),
	controllerDir = __dirname + '/controllers',
	files 				= fs.readdirSync(controllerDir),
	controllers 	= {};

// Dynamically getting the controllers
for (var i = files.length - 1; i >= 0; i--) {
	var controller = files[i].replace('.js','');
	controllers[controller] = require(controllerDir + '/' + controller);
};

function create_systems () {
	var systems	= fs.readdirSync(),
}