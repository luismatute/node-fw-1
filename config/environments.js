var
	splunk = {
		APIURL: "https://api.8zba-eef2.data.splunkstorm.com/1/inputs/http",
		token: "1fn7FhCvqdJnc0QAYcb3FceJynYIL8qDEtpc7KVMtaqN5O8wG1hTOq9dGO7SOz5wQs9HifWMjBc=",
		projectID: "7d6a0cf033d011e3b63422000a1cdcf0"
	};

module.exports = {
	// Development Configs
	  development: {
	  	// App Settings
	  	port: process.env.PORT || 3000,
	  	title: 'Skeleton-Dev',

	  	// Mongo
	  	mongo: {
				dsn: "fluky",
				username: "",
				password: "",
				hosts: [ {serverName:'mongodb://mongodb01.flukyfactory.com',serverPort:'27017'} ]
			},
			// Git
			git: {
				branch: "dev",
				project_name: ""
			},
			// Splunk
			splunk: splunk,
			emailList: ["dbarahona@flukyfactory.com","lbarahona@flukyfactory.com","agutierrez@flukyfactory.com","lmatute@flukyfactory.com"],
			keyMandrillAPI: "mpJ1ejceRDOrZiaDSIKjYg",
	  },

  // Production Configs
	  production: {
	  	port: process.env.PORT || 8080,
	  	title: 'Skeleton-Prod',
	  	// Mongo
	  	mongo: {
				dsn: "fluky",
				username: "",
				password: "",
				hosts: [ {serverName:'mongodb://mongodb01.flukyfactory.com',serverPort:'27017'} ]
			},
			// Git
			git: {
				branch: "dev",
				project_name: ""
			},
			// Splunk
			splunk: splunk,
			emailList: ["dbarahona@flukyfactory.com","lbarahona@flukyfactory.com","agutierrez@flukyfactory.com","lmatute@flukyfactory.com"],
			keyMandrillAPI: "mpJ1ejceRDOrZiaDSIKjYg",
		}
};