// Module Dependencies
var path	= require('path');

// Controllers
var
	admin 	= require('../systems/admin/controllerWrapper'),
	front 	= require('../systems/front/controllerWrapper');

// Expose Routes
module.exports = function(app) {
	// Public Routes
 	app
 		.set('views', path.join(__dirname, '../systems/front/views'))
 		.use('/front/assets', path.join(__dirname, '../systems/front/assets'));
 	app.get('/', front.home.index); // System index
 	Object.keys(front).forEach(function(section) {
		Object.keys(front[section]).forEach(function(item) {
	    app.get('/' + section + '/' + item, front[section][item]);
		});
	});

 	// Admin Routes
	app
		.set('views', path.join(__dirname, '../systems/admin/views'))
		.use('/admin/assets', path.join(__dirname, '../systems/admin/assets'));
 	app.get('/admin', admin.home.index); // System index
 	Object.keys(admin).forEach(function(section) {
		Object.keys(admin[section]).forEach(function(item) {
	    app.get('/' + section + '/' + item, admin[section][item]);
		});
	});
};