'use strict';

module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Less
		less: {
			development: {
				options: {
					paths: ["public/css"],
					compress: true,
					yuicompress: true,
					optimization: 2
				},
				files: {
					"public/css/master.css": "public/less/master.less"
				}
			},
			production: {
				options: {
					paths: ["public/css"],
					cleancss: true
				},
				files: {
					"public/css/master.css": "public/less/master.less"
				}
			}
		},

		watch: {
			styles: {
				// Which files to watch (all .less files recursively in the less directory)
				files: ['public/less/**/*.less'],
		        tasks: ['less'],
		        options: {
		          nospawn: true
		        }
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['watch']);
}