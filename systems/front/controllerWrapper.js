/*
	ControllersWrapper.js
	This file is in charge of dynamically creating the controllers for this system.
	It automatically reads the files from the ./controllers directory
*/

var
	fs 						= require('fs'),
	controllerDir = __dirname + '/controllers',
	files 				= fs.readdirSync(controllerDir),
	controllers 	= {};

// Dynamically getting the controllers
for (var i = files.length - 1; i >= 0; i--) {
	var controller = files[i].replace('.js','');
	controllers[controller] = require(controllerDir + '/' + controller);
};

module.exports = controllers